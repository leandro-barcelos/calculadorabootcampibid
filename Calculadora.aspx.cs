﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CalculadoraBootcampIBID
{
    public partial class Calculadora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Clear_Textbox()
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
        }

        protected void CalcularBtn_Click(object sender, EventArgs e)
        {
            float num_1;
            float num_2;

            if (!float.TryParse(TextBox1.Text, out num_1) || !float.TryParse(TextBox2.Text, out num_2))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Número inválido')", true);
                DropDownList1.SelectedIndex = 0;
                Clear_Textbox();
                return;
            }

            Clear_Textbox();

            float resultado;

            switch (DropDownList1.SelectedIndex)
            {
                case 1:
                    resultado = num_1 + num_2;
                    break;
                case 2:
                    resultado = num_1 - num_2;
                    break;
                case 3:
                    if (num_2 == 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Divisão por zero')", true);
                        DropDownList1.SelectedIndex = 0;
                        Clear_Textbox();
                        return;
                    }
                    resultado = num_1 / num_2;
                    break;
                case 4:
                    resultado = num_1 * num_2;
                    break;
                default:
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Selecione uma operação')", true);
                    return;
            }

            DropDownList1.SelectedIndex = 0;
            Label1.Text = resultado.ToString();
            Label1.Visible = true;
        }
    }
}