﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculadora.aspx.cs" Inherits="CalculadoraBootcampIBID.Calculadora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="calculadora">
            <table style="width: 100%;">
                <tr>
                    <td colspan="2" style="text-align: center"><h1>CALCULADORA</h1></td>
                </tr>
                <tr>
                    <td class="indicador">Primeiro Número:</td>
                    <td><asp:TextBox ID="TextBox1" runat="server" CssClass="textbox"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server">
                            <asp:ListItem Selected="True" Value=" "></asp:ListItem>
                            <asp:ListItem Value="+">+</asp:ListItem>
                            <asp:ListItem Value="-">-</asp:ListItem>
                            <asp:ListItem Value="/">/</asp:ListItem>
                            <asp:ListItem Value="*">*</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="indicador">Segundo Número:</td>
                    <td><asp:TextBox ID="TextBox2" runat="server" CssClass="textbox"></asp:TextBox></td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td class="indicador">Resultado:</td>
                    <td style="padding: 10px"><asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center"><asp:Button ID="Button1" runat="server" Text="Calcular" OnClick="CalcularBtn_Click" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
